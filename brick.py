import sys
def brick(width1):
    a = width1 * "*"
    b = "*" +(" " * (width1 - 2)) + "*"
    print(a + "\n" + b + "\n" + a)
def build(height, width, type3, type2, type1):
    
    wallwidth = height
    brick_widths = [3, 2, 1]
    brick_numbers = [type3, type2, type1]
    wall = []
    for i in range(len(brick_widths)):
        brick_width = brick_widths[i]
        brick_number = brick_numbers[i]
        while wallwidth >= brick_width and brick_number > 0:
            wallwidth -= brick_width
            brick_number -= 1
            wall.append(brick_width)
            if brick_width == 3:
                type3 -= 1
            elif brick_width ==2:
                type2 -= 1
            else:
                type1 -= 1
    if wallwidth == 0:
        line1 = ""
        line2 = ""
        for j in range(len(wall)):
            line1 += "*****" * wall[j]
            line2 += "*" + (" " * ((wall[j]*5)-2))+"*"
        print(line1)
        print(line2)
        print(line1)
    return type3, type2, type1
    
height=int(sys.argv[1])
width=int(sys.argv[2])
type3=int(sys.argv[3])
type2=int(sys.argv[4])
type1=int(sys.argv[5])


if height*width <= (3*type3 + 2*type2 + type1):
    print("Type 3 brick ")
    (brick(15))
    print("Type 2 brick ")
    (brick(10))
    print("Type 1 brick ")
    (brick(5))
    print("\nThe Wall")
    
    while (type3 + type2 + type1 > 0) and width != 0:
    
        type3, type2, type1 = build(height, width, type3, type2, type1)

else:
    print("CAN NOT BE DONE")